package com.example.lucas.imob_admin.cliente;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.example.lucas.imob_admin.R;
import com.example.lucas.imob_admin.api.ApiClient;
import com.example.lucas.imob_admin.api.ApiService;
import com.example.lucas.imob_admin.models.Cliente;

public abstract class ClienteBaseActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "ClienteBaseActivity";

    private ApiService api = ApiClient.createService(ApiService.class);

    private ClienteViewModelBase viewModel;
    private Toolbar toolbar;
    private LinearLayout layout;
    protected EditText nome, telefone;
    private Button submit;
    private TextInputLayout tilNome, tilTelefone;
    private ProgressBar progressBar;

    private String btnLabel;
    private int clientId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cliente_form);

        setupViewModel();

        toolbar = findViewById(R.id.toolbarClienteForm);
        toolbar.setTitle(getString(getBtnLabel()) + " Cliente");
        setSupportActionBar(toolbar);

        layout = findViewById(R.id.linLayClienteForm);

        nome = findViewById(R.id.nome_cliente);
        telefone = findViewById(R.id.telefone_cliente);
        telefone.addTextChangedListener(new PhoneNumberFormattingTextWatcher("BR"));

        submit = findViewById(R.id.btn_criar_cliente);
        submit.setText(getString(getBtnLabel()));
        submit.setOnClickListener(this);

        progressBar = findViewById(R.id.progressBarCliente);

        tilNome = findViewById(R.id.tilNome);
        tilTelefone = findViewById(R.id.tilTelefone);
    }

    public abstract int getBtnLabel();

    public abstract Class<? extends ClienteViewModelBase> getViewModelClass();

    public abstract Cliente getCliente();

    private void setupViewModel() {
        viewModel = ViewModelProviders.of(this).get(getViewModelClass());

        viewModel.getmCliente().observe(this, new Observer<Cliente>() {
            @Override
            public void onChanged(@Nullable Cliente cliente) {
                if (cliente != null) {
                    Intent output = new Intent();
                    setResult(RESULT_OK, output);
                    finish();
                }
            }
        });

        viewModel.getLoading().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if (aBoolean) {
                    submit.setText("");
                    submit.setEnabled(false);
                    progressBar.setVisibility(View.VISIBLE);
                } else {
                    submit.setText(getString(getBtnLabel()));
                    submit.setEnabled(true);
                    progressBar.setVisibility(View.GONE);
                }
            }
        });

        viewModel.getError().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if (aBoolean) {
                    submit.setText(btnLabel);
                    submit.setEnabled(true);
                    progressBar.setVisibility(View.GONE);
                    Snackbar.make(layout, "Erro, tente novamente", Snackbar.LENGTH_LONG).show();
                }
            }
        });

    }

    private boolean validate() {
        if (nome.getText().toString().equals("")) {
            tilNome.setError("Campo obrigatorio");
            return false;
        }
        if (telefone.getText().toString().equals("")) {
            tilTelefone.setError("Campo obrigatorio");
            return false;
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            default: break;
            case R.id.btn_criar_cliente:
                if (validate()) {
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(layout.getWindowToken(), 0); // hide keyboard
                    viewModel.submit(getCliente());
                }
                break;
        }
    }
}
