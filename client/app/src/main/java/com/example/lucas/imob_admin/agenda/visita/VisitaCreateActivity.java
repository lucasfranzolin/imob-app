package com.example.lucas.imob_admin.agenda.visita;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.button.MaterialButton;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TimePicker;

import com.example.lucas.imob_admin.R;
import com.example.lucas.imob_admin.imovel.ImovelDetailActivity;
import com.example.lucas.imob_admin.api.ApiClient;
import com.example.lucas.imob_admin.api.ApiService;
import com.example.lucas.imob_admin.api.RetrofitCallback;
import com.example.lucas.imob_admin.models.Cliente;
import com.example.lucas.imob_admin.models.Visita;

import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class VisitaCreateActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "VisitaCreateActivity";

    private ApiService api = ApiClient.createService(ApiService.class);

    private ConstraintLayout layout;
    private VisitaCreateViewModel viewModel;
    private Spinner spinnVisitante;
    private EditText pickDay, pickTime;
    private MaterialButton submit;
    private ProgressBar progressBar;

    private String date, time;
    private Locale meuLocal = new Locale( "pt", "BR" );
    private int imovelId;
    private Cliente visitante;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_visita);

        setupViewModel();

        layout = findViewById(R.id.layout_agendar_visita);

        spinnVisitante = findViewById(R.id.spinn_visitante);
        spinnVisitante.setEnabled(false);

        pickDay = findViewById(R.id.input_day);
        pickTime = findViewById(R.id.input_time);

        pickDay.setOnClickListener(this);
        pickTime.setOnClickListener(this);

        submit = findViewById(R.id.btn_agendar_visita);
        submit.setOnClickListener(this);

        progressBar = findViewById(R.id.progressBarCreateVisita);

        getListOfClientes();
    }

    private void setupViewModel() {
        viewModel = ViewModelProviders.of(this).get(VisitaCreateViewModel.class);

        viewModel.getmVisita().observe(this, new Observer<Visita>() {
            @Override
            public void onChanged(@Nullable Visita visita) {
                finish();
            }
        });

        viewModel.getLoading().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if (aBoolean) {
                    submit.setText("");
                    submit.setEnabled(false);
                    progressBar.setVisibility(View.VISIBLE);
                } else {
                    submit.setText("Agendar");
                    submit.setEnabled(true);
                    progressBar.setVisibility(View.GONE);
                }
            }
        });

        viewModel.getError().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if (aBoolean) {
                    submit.setText("Agendar");
                    submit.setEnabled(true);
                    progressBar.setVisibility(View.GONE);
                }
            }
        });
    }

    private void getListOfClientes() {
        api.listClientes().enqueue(new RetrofitCallback<List<Cliente>>() {
            @Override
            public void onSuccess(List<Cliente> model) {
                spinnVisitante.setEnabled(true);
                ArrayAdapter<Cliente> adapter = new ArrayAdapter<Cliente>(VisitaCreateActivity.this, android.R.layout.simple_spinner_item, model);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinnVisitante.setAdapter(adapter);
                spinnVisitante.setSelection(0);
                spinnVisitante.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        visitante = (Cliente) parent.getSelectedItem();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }

            @Override
            public void onFailure(int code, String msg) {

            }

            @Override
            public void onThrowable(Throwable t) {

            }

            @Override
            public void onFinish() {

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.input_day:
                DatePickerDialog.OnDateSetListener onDateSetListener = new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth) {
                        date = String.format(meuLocal, "%02d-%02d-%02d", year, month+1, dayOfMonth);
                        pickDay.setText(date);
                    }
                };

                Calendar today = Calendar.getInstance();
                int year = today.get(java.util.Calendar.YEAR);
                int month = today.get(java.util.Calendar.MONTH);
                int day = today.get(java.util.Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(this, onDateSetListener, year, month, day);
                datePickerDialog.show();
                break;

            case R.id.input_time:
                TimePickerDialog.OnTimeSetListener onTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hour, int minute) {
                        time = String.format(meuLocal, "%02d:%02d", hour, minute);
                        pickTime.setText(time);
                    }
                };

                Calendar now = Calendar.getInstance();
                int hour = now.get(java.util.Calendar.HOUR_OF_DAY);
                int minute = now.get(java.util.Calendar.MINUTE);

                TimePickerDialog timePickerDialog = new TimePickerDialog(this, onTimeSetListener, hour, minute, true);
                timePickerDialog.show();
                break;
            case R.id.btn_agendar_visita:
                Visita visita = new Visita(date, time, visitante.getId(), imovelId);
                viewModel.create(visita);
                break;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Bundle bundle = getIntent().getExtras();
        imovelId = bundle.getInt(ImovelDetailActivity.IMOVEL_DETAIL_KEY);
    }

}
