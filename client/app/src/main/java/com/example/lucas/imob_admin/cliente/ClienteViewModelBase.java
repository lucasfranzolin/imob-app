package com.example.lucas.imob_admin.cliente;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import androidx.annotation.NonNull;

import com.example.lucas.imob_admin.api.ApiClient;
import com.example.lucas.imob_admin.api.ApiService;
import com.example.lucas.imob_admin.models.Cliente;

public abstract class ClienteViewModelBase extends AndroidViewModel {

    protected ApiService api = ApiClient.createService(ApiService.class);

    protected MutableLiveData<Cliente> mCliente = new MutableLiveData<>();
    protected MutableLiveData<Boolean> loading = new MutableLiveData<>();
    protected MutableLiveData<Boolean> error = new MutableLiveData<>();

    public ClienteViewModelBase(@NonNull Application application) {
        super(application);
    }

    public abstract void submit(final Cliente cliente);

    public MutableLiveData<Cliente> getmCliente() {
        return mCliente;
    }

    public MutableLiveData<Boolean> getLoading() {
        return loading;
    }

    public MutableLiveData<Boolean> getError() {
        return error;
    }

}
