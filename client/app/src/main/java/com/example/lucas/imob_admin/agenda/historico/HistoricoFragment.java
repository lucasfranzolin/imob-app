package com.example.lucas.imob_admin.agenda.historico;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.lucas.imob_admin.R;
import com.example.lucas.imob_admin.agenda.visita.VisitaAdapter;
import com.example.lucas.imob_admin.models.Visita;
import com.example.lucas.imob_admin.main.MainViewModel;

import java.util.List;

public class HistoricoFragment extends Fragment implements View.OnClickListener, VisitaAdapter.ClickListener, SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = "HistoricoFragment";
    private MainViewModel viewModel;
    private SwipeRefreshLayout swipe;
    private RecyclerView recyclerView;
    private VisitaAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_visitas, container, false);

        setupViewModel();

        swipe = view.findViewById(R.id.swipeVisitas);
        swipe.setOnRefreshListener(this);
        swipe.setColorSchemeResources(R.color.primaryColor,
                R.color.secondaryColor);

        recyclerView = view.findViewById(R.id.recycler_visitas);
        recyclerView.setHasFixedSize(true);
        adapter = new VisitaAdapter(getActivity());
        adapter.setOnItemClickListener(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        return view;
    }

    private void setupViewModel() {
        viewModel = ViewModelProviders.of(getActivity()).get(MainViewModel.class);

        viewModel.getOldVisitas().observe(this, new Observer<List<Visita>>() {
            @Override
            public void onChanged(@Nullable List<Visita> visitas) {
                adapter.setItems(visitas);
            }
        });

        viewModel.getLoading().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if (aBoolean) {
                    swipe.setRefreshing(true);
                } else {
                    swipe.setRefreshing(false);
                }
            }
        });

        viewModel.getError().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if (aBoolean) swipe.setRefreshing(false);
            }
        });

    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onItemClick(int position, View v) {

    }

    @Override
    public void onItemDeleteClick(int position) {
        Log.d(TAG, "onItemDeleteClick: " + position);
        viewModel.deleteVisita(adapter.getItem(position).getId());
    }

    @Override
    public void onRefresh() {
        viewModel.listOldVisitas();
    }

}
