package com.example.lucas.imob_admin.imovel;

import android.app.Activity;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.lucas.imob_admin.R;
import com.example.lucas.imob_admin.api.ApiClient;
import com.example.lucas.imob_admin.api.ApiService;
import com.example.lucas.imob_admin.api.RetrofitCallback;
import com.example.lucas.imob_admin.models.Cliente;
import com.example.lucas.imob_admin.models.Imovel;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.travijuu.numberpicker.library.NumberPicker;

import java.util.ArrayList;
import java.util.List;

import static com.example.lucas.imob_admin.imovel.ImovelDetailActivity.UPDATE_IMOVEL_REQUEST_CODE;
import static com.example.lucas.imob_admin.imovel.ImovelFragment.FORM_IMOVEL_REQUEST_CODE;

public abstract class ImovelBaseActivity extends AppCompatActivity implements View.OnClickListener    {
    private static final String TAG = "ImovelFormActivity";

    public static final int PICK_IMAGE_MULTIPLE = 6950;

    private ApiService api = ApiClient.createService(ApiService.class);

    private List<Uri> mArrayUri;

    private ImovelViewModelBase viewModel;
    private CoordinatorLayout layout;
    protected Spinner spinnTipo, spinnTopologia, spinnProprietario;
    protected RadioGroup rg_condicao;
    protected RadioButton rb;
    protected TextInputEditText preco_imovel, areaTotal_imovel, areaConstruida_imovel, condominio_imovel, end_estado, end_cidade, end_bairro, end_rua, end_numero, end_comp;
    protected TextInputLayout tilPreco, tilCondominio, tilAreaTotal, tilAreaConstruida, tilEstado, tilCidade, tilBairro, tilRua, tilNumero, tilComplemento;
    protected NumberPicker garagem_np, quarto_np, banheiro_np;
    protected MaterialButton submit, btnFotos;
    protected CheckBox mobiliado;
    protected ProgressBar progressBar;
    protected Cliente proprietario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imovel_form);

        setupViewModel();

        layout = findViewById(R.id.coodinatorImovel);

        spinnTipo = findViewById(R.id.imovel_spinner);
        ArrayAdapter<CharSequence> adapterTipo = ArrayAdapter.createFromResource(this,
                R.array.tipo_imovel, android.R.layout.simple_spinner_item);
        adapterTipo.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnTipo.setAdapter(adapterTipo);

        rg_condicao = findViewById(R.id.rg_condicao);

        tilPreco = findViewById(R.id.tilPreco);
        tilCondominio = findViewById(R.id.tilCondominio);
        tilAreaTotal = findViewById(R.id.tilAreaTotal);
        tilAreaConstruida = findViewById(R.id.tilAreaConstruida);
        tilEstado = findViewById(R.id.tilEstado);
        tilCidade = findViewById(R.id.tilCidade);
        tilBairro = findViewById(R.id.tilBairro);
        tilRua = findViewById(R.id.tilRua);
        tilNumero = findViewById(R.id.tilNumero);
        tilComplemento = findViewById(R.id.tilComplemento);

        preco_imovel = findViewById(R.id.preco_imovel);
        areaTotal_imovel = findViewById(R.id.areaTotal_imovel);
        areaConstruida_imovel = findViewById(R.id.areaConstruida_imovel);
        condominio_imovel = findViewById(R.id.condominio_imovel);
        mobiliado = findViewById(R.id.is_mobiliado);
        end_estado = findViewById(R.id.end_estado);
        end_cidade = findViewById(R.id.end_cidade);
        end_bairro = findViewById(R.id.end_bairro);
        end_rua = findViewById(R.id.end_rua);
        end_numero = findViewById(R.id.end_numero);
        end_comp = findViewById(R.id.end_comp);

        spinnTopologia = findViewById(R.id.topologia);
        ArrayAdapter<CharSequence> adapterTopologia = ArrayAdapter.createFromResource(this,
                R.array.topologia_imovel, android.R.layout.simple_spinner_item);
        adapterTopologia.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnTopologia.setAdapter(adapterTopologia);

        garagem_np = findViewById(R.id.garagem_np);
        garagem_np.setMin(0);
        garagem_np.setMax(99);
        garagem_np.setUnit(1);
        garagem_np.setValue(0);

        quarto_np = findViewById(R.id.quarto_np);
        quarto_np.setMin(0);
        quarto_np.setMax(99);
        quarto_np.setUnit(1);
        quarto_np.setValue(0);

        banheiro_np = findViewById(R.id.banheiro_np);
        banheiro_np.setMin(0);
        banheiro_np.setMax(99);
        banheiro_np.setUnit(1);
        banheiro_np.setValue(0);

        submit = findViewById(R.id.btn_criar_imovel);
        submit.setText(getBtnLabel());
        submit.setOnClickListener(this);

        btnFotos = findViewById(R.id.btn_select_foto);
        btnFotos.setOnClickListener(this);

        progressBar = findViewById(R.id.progressBarImovel);

        spinnProprietario = findViewById(R.id.proprietario);
        spinnProprietario.setEnabled(false);
        getListOfClientes();
    }

    public abstract int getBtnLabel();

    public abstract Class<? extends ImovelViewModelBase> getViewModelClass();

    public abstract Imovel getImovel();

    private void setupViewModel() {
        viewModel = ViewModelProviders.of(this).get(getViewModelClass());

        viewModel.getmImovel().observe(this, new Observer<Imovel>() {
            @Override
            public void onChanged(@Nullable Imovel imovel) {
                if (imovel != null) {
                    Intent output = new Intent();
                    setResult(RESULT_OK, output);
                    finish();
                }
            }
        });

        viewModel.getLoading().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if (aBoolean) {
                    submit.setText("");
                    submit.setEnabled(false);
                    progressBar.setVisibility(View.VISIBLE);
                } else {
                    submit.setText(R.string.btn_create);
                    submit.setEnabled(true);
                    progressBar.setVisibility(View.GONE);
                }
            }
        });

        viewModel.getError().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if (aBoolean) {
                    submit.setText(R.string.btn_create);
                    submit.setEnabled(true);
                    progressBar.setVisibility(View.GONE);
                    Snackbar.make(layout, "Erro, tente novamente", Snackbar.LENGTH_LONG).show();
                }
            }
        });

    }

    private void getListOfClientes() {
        api.listClientes().enqueue(new RetrofitCallback<List<Cliente>>() {
            @Override
            public void onSuccess(List<Cliente> model) {
                spinnProprietario.setEnabled(true);
                ArrayAdapter<Cliente> adapter = new ArrayAdapter<Cliente>(ImovelBaseActivity.this, android.R.layout.simple_spinner_item, model);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinnProprietario.setAdapter(adapter);
                spinnProprietario.setSelection(0);
                spinnProprietario.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        proprietario = (Cliente) parent.getSelectedItem();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }

            @Override
            public void onFailure(int code, String msg) {

            }

            @Override
            public void onThrowable(Throwable t) {

            }

            @Override
            public void onFinish() {

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            default: break;
            case R.id.btn_criar_imovel:
                rb = findViewById(rg_condicao.getCheckedRadioButtonId());
                submit.setEnabled(false);
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(layout.getWindowToken(), 0); // hide keyboard
                viewModel.submit(getImovel());
                break;
            case R.id.btn_select_foto:
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent,"Selecione as fotos"), PICK_IMAGE_MULTIPLE);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        try {
            if (resultCode == RESULT_OK) {
                switch (requestCode) {
                    case PICK_IMAGE_MULTIPLE:
                        if (null != data) {
                            mArrayUri = new ArrayList<>();
                            if (data.getData() != null) {
                                mArrayUri.add(data.getData());
                            } else {
                                if (data.getClipData() != null) {
                                    ClipData mClipData = data.getClipData();
                                    for (int i = 0; i < mClipData.getItemCount(); i++) {
                                        mArrayUri.add(mClipData.getItemAt(i).getUri());
                                    }
                                }
                            }
                            viewModel.setImageUris(mArrayUri);
                        } else {
                            Snackbar.make(layout, "Nenhuma imagem selecionada", Snackbar.LENGTH_LONG).show();
                        }
                        break;

                    default:
                        break;
                }
            }
        } catch (Exception e) {
            Snackbar.make(layout, "Erro", Snackbar.LENGTH_LONG).show();
        }

        super.onActivityResult(requestCode, resultCode, data);
    }
}