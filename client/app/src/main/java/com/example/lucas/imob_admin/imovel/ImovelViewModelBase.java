package com.example.lucas.imob_admin.imovel;

import android.app.Application;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.example.lucas.imob_admin.api.ApiClient;
import com.example.lucas.imob_admin.api.ApiService;
import com.example.lucas.imob_admin.models.Imovel;

import java.util.ArrayList;
import java.util.List;

public abstract class ImovelViewModelBase extends AndroidViewModel {

    protected ApiService api = ApiClient.createService(ApiService.class);

    protected List<Uri> imageUris = new ArrayList<>();

    protected MutableLiveData<Imovel> mImovel = new MutableLiveData<>();
    protected MutableLiveData<Boolean> loading = new MutableLiveData<>();
    protected MutableLiveData<Boolean> error = new MutableLiveData<>();

    public ImovelViewModelBase(@NonNull Application application) {
        super(application);
    }

    public abstract void submit(final Imovel imovel);

    public MutableLiveData<Imovel> getmImovel() {
        return mImovel;
    }

    public MutableLiveData<Boolean> getLoading() {
        return loading;
    }

    public MutableLiveData<Boolean> getError() {
        return error;
    }

    public void setImageUris(List<Uri> imageUris) {
        this.imageUris = imageUris;
    }

}
