package com.example.lucas.imob_admin.main;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.annotation.NonNull;
import androidx.annotation.UiThread;

import com.example.lucas.imob_admin.api.ApiClient;
import com.example.lucas.imob_admin.api.ApiService;
import com.example.lucas.imob_admin.models.Cliente;
import com.example.lucas.imob_admin.models.Imovel;
import com.example.lucas.imob_admin.api.RetrofitCallback;
import com.example.lucas.imob_admin.models.Visita;

import java.util.List;

public class MainViewModel extends AndroidViewModel {
    private static final String TAG = "MainViewModel";

    private ApiService api = ApiClient.createService(ApiService.class);

    private MutableLiveData<List<Imovel>> imoveis = new MutableLiveData<>();
    private MutableLiveData<List<Visita>> newVisitas = new MutableLiveData<>();
    private MutableLiveData<List<Visita>> oldVisitas = new MutableLiveData<>();
    private MutableLiveData<List<Cliente>> clientes = new MutableLiveData<>();

    private MutableLiveData<Boolean> loading = new MutableLiveData<>();
    private MutableLiveData<Boolean> error = new MutableLiveData<>();

    private int count;

    @UiThread
    public MainViewModel(@NonNull Application application) {
        super(application);
    }

    // [START] Cliente
    public void listClientes() {
        loading.setValue(true);
        api.listClientes().enqueue(new RetrofitCallback<List<Cliente>>() {
            @Override
            public void onSuccess(List<Cliente> model) {
                clientes.setValue(model);
                error.setValue(false);
            }

            @Override
            public void onFailure(int code, String msg) {
                error.setValue(true);
            }

            @Override
            public void onThrowable(Throwable t) {
                error.setValue(true);
            }

            @Override
            public void onFinish() {
                loading.setValue(false);
            }
        });
    }

    private void deleteOne(final Cliente cliente) {
        loading.setValue(true);
        api.deleteCliente(cliente.getId()).enqueue(new RetrofitCallback<String>() {
            @Override
            public void onSuccess(String model) {
                if (count == 0) {
                    error.setValue(false);
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                error.setValue(true);
            }

            @Override
            public void onThrowable(Throwable t) {
                error.setValue(true);
            }

            @Override
            public void onFinish() {
                count--;
                if (count == 0) {
                    loading.setValue(false);
                }
            }
        });
    }

    public void deleteClientes(final List<Cliente> clientes) {
        count = clientes.size();
        for (int i = 0; i < count; i++) {
            deleteOne(clientes.get(i));
        }
    }

    public LiveData<List<Cliente>> getClientes() {
        return clientes;
    }
    // [END] Cliente

    // [START] Imovel
    public void listImoveis() {
        loading.setValue(true);
        api.listImoveis().enqueue(new RetrofitCallback<List<Imovel>>() {
            @Override
            public void onSuccess(List<Imovel> model) {
                imoveis.setValue(model);
                error.setValue(false);
            }

            @Override
            public void onFailure(int code, String msg) {
                error.setValue(true);
            }

            @Override
            public void onThrowable(Throwable t) {
                error.setValue(true);
            }

            @Override
            public void onFinish() {
                loading.setValue(false);
            }
        });
    }

    public LiveData<List<Imovel>> getImoveis() {
        return imoveis;
    }
    // [END] Imovel

    // [START] Visita
    public void listNewVisitas() {
        loading.setValue(true);
        api.listNewVisitas().enqueue(new RetrofitCallback<List<Visita>>() {
            @Override
            public void onSuccess(List<Visita> model) {
                newVisitas.setValue(model);
                error.setValue(false);
            }

            @Override
            public void onFailure(int code, String msg) {
                error.setValue(true);
            }

            @Override
            public void onThrowable(Throwable t) {
                error.setValue(true);
            }

            @Override
            public void onFinish() {
                loading.setValue(false);
            }
        });
    }

    public void listOldVisitas() {
        loading.setValue(true);
        api.listOldVisitas().enqueue(new RetrofitCallback<List<Visita>>() {
            @Override
            public void onSuccess(List<Visita> model) {
                oldVisitas.setValue(model);
                error.setValue(false);
            }

            @Override
            public void onFailure(int code, String msg) {
                error.setValue(true);
            }

            @Override
            public void onThrowable(Throwable t) {
                error.setValue(true);
            }

            @Override
            public void onFinish() {
                loading.setValue(false);
            }
        });
    }

    public void deleteVisita(final int id) {
        loading.setValue(true);
        api.deleteVisita(id).enqueue(new RetrofitCallback<String>() {
            @Override
            public void onSuccess(String model) {
                error.setValue(false);
            }

            @Override
            public void onFailure(int code, String msg) {
                error.setValue(true);
            }

            @Override
            public void onThrowable(Throwable t) {
                error.setValue(true);
            }

            @Override
            public void onFinish() {
                if (!error.getValue()) {
                    listNewVisitas();
                } else {
                    loading.setValue(false);
                }
            }
        });
    }

    public MutableLiveData<List<Visita>> getNewVisitas() {
        return newVisitas;
    }

    public MutableLiveData<List<Visita>> getOldVisitas() {
        return oldVisitas;
    }

    // [END] Visita

    public MutableLiveData<Boolean> getLoading() {
        return loading;
    }

    public MutableLiveData<Boolean> getError() {
        return error;
    }

    @Override
    protected void onCleared() {
        super.onCleared();
    }
}
