package com.example.lucas.imob_admin.cliente;

import android.app.Application;
import androidx.annotation.NonNull;

import com.example.lucas.imob_admin.api.RetrofitCallback;
import com.example.lucas.imob_admin.models.Cliente;

public class ClienteCreateViewModel extends ClienteViewModelBase {

    public ClienteCreateViewModel(@NonNull Application application) {
        super(application);
    }

    @Override
    public void submit(Cliente cliente) {
        loading.setValue(true);
        api.createCliente(cliente).enqueue(new RetrofitCallback<Cliente>() {
            @Override
            public void onSuccess(Cliente model) {
                mCliente.setValue(model);
            }

            @Override
            public void onFailure(int code, String msg) {
                error.setValue(true);
            }

            @Override
            public void onThrowable(Throwable t) {
                error.setValue(true);
            }

            @Override
            public void onFinish() {
                loading.setValue(false);
            }
        });
    }
}
