package com.example.lucas.imob_admin.cliente;

import android.os.Bundle;

import com.example.lucas.imob_admin.R;
import com.example.lucas.imob_admin.models.Cliente;

public class ClienteUpdateActivity extends ClienteBaseActivity {
    public static final String CLIENTE_EXTRA = "cliente_obj";

    private Cliente cliente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle data = getIntent().getExtras();
        cliente = (Cliente) data.getParcelable(CLIENTE_EXTRA);
        nome.setText(cliente.getNome());
        telefone.setText(cliente.getTelefone());
    }

    @Override
    public int getBtnLabel() {
        return R.string.btn_update;
    }

    @Override
    public Class<? extends ClienteViewModelBase> getViewModelClass() {
        return ClienteUpdateViewModel.class;
    }

    @Override
    public Cliente getCliente() {
        Cliente clienteEdited = new Cliente(nome.getText().toString(), telefone.getText().toString());
        clienteEdited.setId(cliente.getId());
        return clienteEdited;
    }

}
