package com.example.lucas.imob_admin.imovel;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.lucas.imob_admin.R;
import com.example.lucas.imob_admin.agenda.visita.VisitaCreateActivity;
import com.example.lucas.imob_admin.models.Cliente;
import com.example.lucas.imob_admin.models.Foto;
import com.example.lucas.imob_admin.models.Imovel;
import com.smarteist.autoimageslider.DefaultSliderView;
import com.smarteist.autoimageslider.SliderLayout;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import static com.example.lucas.imob_admin.imovel.ImovelFragment.IMOVEL_FRAGMENT_KEY;

public class ImovelDetailActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "ImovelDetailActivity";

    public static final String IMOVEL_DETAIL_KEY = "imovel_id";
    private static final int IMOVEL_SCHEDULE_CODE = 123;
    public static final int UPDATE_IMOVEL_REQUEST_CODE = 2;

    private ProgressBar progressBar;
    private RelativeLayout layout;
    private ImovelDetailViewModel viewModel;
    private Toolbar toolbar;
    private TextView preco, condominio, enderecoFull, enderecoLabel, tipo;
    private TextView proprietario, condicao, areaTotal, areaConstruida, topologia, mobiliado, banheiro, quarto, garagem;
    private SliderLayout sliderLayout;
    private FloatingActionButton btnAgendar;
    private int imovelId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imovel_detail);

        setupViewModel();

        toolbar = findViewById(R.id.toolbar_detail);
        setSupportActionBar(toolbar);

        layout = findViewById(R.id.lay_imovel_detail);

        progressBar = findViewById(R.id.progressBarImovelDetail);
        progressBar.setVisibility(View.GONE);

        sliderLayout = findViewById(R.id.imageSlider);
        sliderLayout.setScrollTimeInSec(10); //set scroll delay in seconds :

        preco = findViewById(R.id.detail_preco2);
        condominio= findViewById(R.id.detail_condominio);
        enderecoLabel = findViewById(R.id.detail_lbl_condominio);
        enderecoFull = findViewById(R.id.detail_full_endereco);
        tipo = findViewById(R.id.detail_tipo);

        proprietario = findViewById(R.id.txt_detail_proprietario);
        condicao = findViewById(R.id.txt_detail_condicao);
        areaTotal = findViewById(R.id.txt_detail_areaTotal);
        areaConstruida = findViewById(R.id.txt_detail_areaConstruida);
        topologia = findViewById(R.id.txt_detail_topologia);
        mobiliado = findViewById(R.id.txt_detail_mobiliado);
        banheiro = findViewById(R.id.txt_detail_banheiro);
        quarto = findViewById(R.id.txt_detail_quarto);
        garagem = findViewById(R.id.txt_detail_garagem);

        btnAgendar = findViewById(R.id.fab_imovel_detail_agendar);
        btnAgendar.setOnClickListener(this);

    }

    private void loadPhotosOnSlider(List<Foto> fotos) {
        for (int i = 0; i < fotos.size(); i++) {
            DefaultSliderView sliderView = new DefaultSliderView(this);
            sliderView.setImageUrl(fotos.get(i).getUrl());
            sliderView.setImageScaleType(ImageView.ScaleType.CENTER_CROP);
            sliderLayout.addSliderView(sliderView);
        }
    }

    private void setupViewModel() {
        viewModel = ViewModelProviders.of(this).get(ImovelDetailViewModel.class);

        viewModel.getmFoto().observe(this, new Observer<List<Foto>>() {
            @Override
            public void onChanged(@Nullable List<Foto> fotos) {
                loadPhotosOnSlider(fotos);
            }
        });

        viewModel.getmImovel().observe(this, new Observer<Imovel>() {
            @Override
            public void onChanged(@Nullable Imovel imovel) {
                loadViewDetails(imovel);
            }
        });

        viewModel.getmProprietario().observe(this, new Observer<Cliente>() {
            @Override
            public void onChanged(@Nullable Cliente cliente) {
                proprietario.setText(cliente.getNome());
            }
        });

        viewModel.getLoading().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if (aBoolean) {
                    progressBar.setVisibility(View.VISIBLE);
                } else {
                    progressBar.setVisibility(View.GONE);
                }
            }
        });

        viewModel.getError().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if (aBoolean) {
                    Snackbar.make(layout, "Erro error", Snackbar.LENGTH_SHORT).show();
                }
            }
        });

        viewModel.getDeleted().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if (!aBoolean) {
                    Snackbar.make(layout, "Erro deleted", Snackbar.LENGTH_SHORT).show();
                } else {
                    finish();
                }
            }
        });
    }

    private void loadViewDetails(Imovel imovel) {
        Locale meuLocal = new Locale( "pt", "BR" );
        NumberFormat format = NumberFormat.getCurrencyInstance(meuLocal);
        preco.setText(format.format(imovel.getPreco()));

        if (imovel.getCondominio() != null && imovel.getCondominio() > 0.0 ) {
            condominio.setText(format.format(imovel.getCondominio()));
        } else {
            condominio.setVisibility(View.GONE);
            enderecoLabel.setVisibility(View.GONE);
        }

        enderecoFull.setText(
                imovel.getRua() + ", " +
                imovel.getNumero().toString() + " - " +
                imovel.getBairro() + ", " +
                imovel.getCidade() + " - " +
                imovel.getEstado()
        );

        viewModel.getProprietario(imovel.getFk_proprietario_id());
        tipo.setText(imovel.getTipo());
        condicao.setText(imovel.getCondicao());
        areaTotal.setText(String.valueOf(imovel.getAreaTotal()) + " m²");
        areaConstruida.setText(String.valueOf(imovel.getAreaConstruida()) + " m²");
        topologia.setText(imovel.getTopologia());
        mobiliado.setText(imovel.getIs_mobiliado() == 1 ? "Sim" : "Nao");
        banheiro.setText(String.valueOf(imovel.getBanheiro()));
        quarto.setText(String.valueOf(imovel.getQuarto()));
        garagem.setText(String.valueOf(imovel.getGaragem()));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab_imovel_detail_agendar:
                Intent intent = new Intent(this, VisitaCreateActivity.class);
                intent.putExtra(IMOVEL_DETAIL_KEY, imovelId);
                startActivity(intent);
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.imovel_detail_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_imovel_edit:
                Intent intent = new Intent(this, ImovelUpdateActivity.class);
                Imovel imovelExtra = viewModel.getmImovel().getValue();
                imovelExtra.setId(imovelId);
                intent.putExtra(ImovelUpdateActivity.IMOVEL_EXTRA, imovelExtra);
                startActivityForResult(intent, UPDATE_IMOVEL_REQUEST_CODE);
                return true;

            case R.id.action_imovel_delete:
                viewModel.deleteImovel(imovelId);
                return true;

            default:
                return false;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Bundle bundle = getIntent().getExtras();
        imovelId = bundle.getInt(IMOVEL_FRAGMENT_KEY);
        viewModel.getImovelDetails(imovelId);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == UPDATE_IMOVEL_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                viewModel.getImovelDetails(imovelId);
            }
        }
    }

}
