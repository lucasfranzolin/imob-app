package com.example.lucas.imob_admin.imovel;

import com.example.lucas.imob_admin.R;
import com.example.lucas.imob_admin.models.Imovel;

public class ImovelCreateActivity extends ImovelBaseActivity {

    @Override
    public int getBtnLabel() {
        return R.string.btn_create;
    }

    @Override
    public Class<? extends ImovelViewModelBase> getViewModelClass() {
        return ImovelCreateViewModel.class;
    }

    @Override
    public Imovel getImovel() {
        Double areaC, areaT, cond, preco;
        Integer num;

        try {
            areaC = Double.valueOf(areaConstruida_imovel.getText().toString());
        } catch (Exception e) {
            areaC = 0.0;
        }

        try {
            areaT = Double.valueOf(areaTotal_imovel.getText().toString());
        } catch (Exception e) {
            areaT = 0.0;
        }

        try {
            cond = Double.valueOf(condominio_imovel.getText().toString());
        } catch (Exception e) {
            cond = 0.0;
        }

        try {
            num = Integer.valueOf(end_numero.getText().toString());
        } catch (Exception e) {
            num = 0;
        }

        try {
            preco = Double.valueOf(preco_imovel.getText().toString());
        } catch (Exception e) {
            preco = 0.0;
        }

        Imovel imovel = new Imovel(
                areaC,
                areaT,
                end_bairro.getText().toString(),
                banheiro_np.getValue(),
                end_cidade.getText().toString(),
                end_comp.getText().toString(),
                rb.getText().toString(),
                cond,
                end_estado.getText().toString(),
                proprietario.getId(),
                garagem_np.getValue(),
                mobiliado.isChecked() ? 1 : 0,
                num,
                preco,
                quarto_np.getValue(),
                end_rua.getText().toString(),
                spinnTipo.getSelectedItem().toString(),
                spinnTopologia.getSelectedItem().toString()
        );
        return imovel;
    }
}
