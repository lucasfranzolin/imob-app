package com.example.lucas.imob_admin.imovel;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.lucas.imob_admin.api.ApiClient;
import com.example.lucas.imob_admin.api.ApiService;
import com.example.lucas.imob_admin.api.RetrofitCallback;
import com.example.lucas.imob_admin.models.Foto;
import com.example.lucas.imob_admin.models.Imovel;
import com.example.lucas.imob_admin.R;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

public class ImovelAdapter extends RecyclerView.Adapter<ImovelAdapter.ViewHolder> {
    private static final String TAG = "ImovelAdapter";
    private ApiService api = ApiClient.createService(ApiService.class);

    private static ClickListener clickListener;

    public interface ClickListener {
        void onItemClick(int position, View v);
    }

    public void setOnItemClickListener(ClickListener clickListener) {
        ImovelAdapter.clickListener = clickListener;
    }

    private List<Imovel> imoveis;
    private Context context;

    public ImovelAdapter(Context context) {
        this.context = context;
        this.imoveis = new ArrayList<>();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tipo, endereco, preco, quarto, banheiro, garagem;
        CircleImageView foto;

        public ViewHolder(@NonNull final View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);

            foto = itemView.findViewById(R.id.imgImovel);
            tipo = itemView.findViewById(R.id.txtTipo);
            preco = itemView.findViewById(R.id.txtPreco);
            endereco = itemView.findViewById(R.id.txtEndereco);
            quarto = itemView.findViewById(R.id.txtQuartos);
            banheiro = itemView.findViewById(R.id.txtBanheiros);
            garagem = itemView.findViewById(R.id.txtGaragens);

        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(getAdapterPosition(), v);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View imovelView = inflater.inflate(R.layout.list_item_imovel, viewGroup, false);
        return new ViewHolder(imovelView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        final Imovel currentImovel = imoveis.get(i);
        setImage(currentImovel, viewHolder.foto);
        viewHolder.tipo.setText(currentImovel.getTipo());

        Locale meuLocal = new Locale( "pt", "BR" );
        NumberFormat format = NumberFormat.getCurrencyInstance(meuLocal);
        viewHolder.preco.setText(format.format(currentImovel.getPreco()));

        // Rua Padre Saturnino - Centro, Lindóia - State of São Paulo
        viewHolder.endereco.setText(
                currentImovel.getRua() + ", " +
                currentImovel.getNumero().toString() + " - " +
                currentImovel.getBairro() + ", " +
                currentImovel.getCidade() + " - " +
                currentImovel.getEstado()
        );
        viewHolder.quarto.setText(String.valueOf(currentImovel.getQuarto()));
        viewHolder.banheiro.setText(String.valueOf(currentImovel.getBanheiro()));
        viewHolder.garagem.setText(String.valueOf(currentImovel.getGaragem()));
    }

    private void setImage(Imovel imovel, final ImageView imageView) {
        api.getFirstFotoUrl(imovel.getId()).enqueue(new RetrofitCallback<Foto>() {
            @Override
            public void onSuccess(Foto model) {
                Glide.with(context).load(model.getUrl()).into(imageView);
            }

            @Override
            public void onFailure(int code, String msg) {

            }

            @Override
            public void onThrowable(Throwable t) {

            }

            @Override
            public void onFinish() {

            }
        });
    }

    @Override
    public int getItemCount() {
        return imoveis.size();
    }

    public void setItems(List<Imovel> imoveis) {
        this.imoveis = imoveis;
        notifyDataSetChanged();
    }

    public Imovel getItem(int position) {
        return imoveis.get(position);
    }

}
