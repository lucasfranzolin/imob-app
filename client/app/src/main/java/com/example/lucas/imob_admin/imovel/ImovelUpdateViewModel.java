package com.example.lucas.imob_admin.imovel;

import android.app.Application;

import androidx.annotation.NonNull;

import com.example.lucas.imob_admin.api.RetrofitCallback;
import com.example.lucas.imob_admin.models.Imovel;

public class ImovelUpdateViewModel extends ImovelViewModelBase {
    public ImovelUpdateViewModel(@NonNull Application application) {
        super(application);
    }

    @Override
    public void submit(Imovel imovel) {
        loading.setValue(true);
        api.updateImovel(imovel.getId(), imovel).enqueue(new RetrofitCallback<Imovel>() {
            @Override
            public void onSuccess(Imovel model) {
                mImovel.setValue(model);
                error.setValue(false);
            }

            @Override
            public void onFailure(int code, String msg) {
                error.setValue(true);
            }

            @Override
            public void onThrowable(Throwable t) {
                error.setValue(true);
            }

            @Override
            public void onFinish() {
                loading.setValue(false);
            }
        });
    }
}
