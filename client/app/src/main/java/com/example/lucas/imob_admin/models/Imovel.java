package com.example.lucas.imob_admin.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Imovel implements Parcelable {

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Imovel createFromParcel(Parcel in) {
            return new Imovel(in);
        }

        public Imovel[] newArray(int size) {
            return new Imovel[size];
        }
    };

    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("fk_proprietario_id")
    @Expose
    public Integer fk_proprietario_id;
    @SerializedName("tipo")
    @Expose
    public String tipo;
    @SerializedName("preco")
    @Expose
    public Double preco;
    @SerializedName("condicao")
    @Expose
    public String condicao;
    @SerializedName("areaTotal")
    @Expose
    public Double areaTotal;
    @SerializedName("areaConstruida")
    @Expose
    public Double areaConstruida;
    @SerializedName("topologia")
    @Expose
    public String topologia;
    @SerializedName("condominio")
    @Expose
    public Double condominio;
    @SerializedName("garagem")
    @Expose
    public Integer garagem;
    @SerializedName("is_mobiliado")
    @Expose
    public Integer is_mobiliado;
    @SerializedName("quarto")
    @Expose
    public Integer quarto;
    @SerializedName("banheiro")
    @Expose
    public Integer banheiro;
    @SerializedName("estado")
    @Expose
    public String estado;
    @SerializedName("cidade")
    @Expose
    public String cidade;
    @SerializedName("bairro")
    @Expose
    public String bairro;
    @SerializedName("rua")
    @Expose
    public String rua;
    @SerializedName("numero")
    @Expose
    public Integer numero;
    @SerializedName("complemento")
    @Expose
    public String complemento;

    public Imovel(Double areaConstruida, Double areaTotal, String bairro, Integer banheiro, String cidade, String complemento, String condicao, Double condominio, String estado, Integer fk_proprietario_id, Integer garagem, Integer is_mobiliado, Integer numero, Double preco, Integer quarto, String rua, String tipo, String topologia) {
        this.areaConstruida = areaConstruida;
        this.areaTotal = areaTotal;
        this.bairro = bairro;
        this.banheiro = banheiro;
        this.cidade = cidade;
        this.complemento = complemento;
        this.condicao = condicao;
        this.condominio = condominio;
        this.estado = estado;
        this.fk_proprietario_id = fk_proprietario_id;
        this.garagem = garagem;
        this.is_mobiliado = is_mobiliado;
        this.numero = numero;
        this.preco = preco;
        this.quarto = quarto;
        this.rua = rua;
        this.tipo = tipo;
        this.topologia = topologia;
    }

    public Imovel(Parcel in){
        this.id = in.readInt();
        this.areaConstruida = in.readDouble();
        this.areaTotal = in.readDouble();
        this.bairro = in.readString();
        this.banheiro = in.readInt();
        this.cidade = in.readString();
        this.complemento = in.readString();
        this.condicao = in.readString();
        this.condominio = in.readDouble();
        this.estado = in.readString();
        this.fk_proprietario_id = in.readInt();
        this.garagem = in.readInt();
        this.is_mobiliado = in.readInt();
        this.numero = in.readInt();
        this.preco = in.readDouble();
        this.quarto = in.readInt();
        this.rua = in.readString();
        this.tipo = in.readString();
        this.topologia = in.readString();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getFk_proprietario_id() {
        return fk_proprietario_id;
    }

    public void setFk_proprietario_id(Integer fk_proprietario_id) {
        this.fk_proprietario_id = fk_proprietario_id;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Double getPreco() {
        return preco;
    }

    public void setPreco(Double preco) {
        this.preco = preco;
    }

    public String getCondicao() {
        return condicao;
    }

    public void setCondicao(String condicao) {
        this.condicao = condicao;
    }

    public Double getAreaTotal() {
        return areaTotal;
    }

    public void setAreaTotal(Double areaTotal) {
        this.areaTotal = areaTotal;
    }

    public Double getAreaConstruida() {
        return areaConstruida;
    }

    public void setAreaConstruida(Double areaConstruida) {
        this.areaConstruida = areaConstruida;
    }

    public String getTopologia() {
        return topologia;
    }

    public void setTopologia(String topologia) {
        this.topologia = topologia;
    }

    public Double getCondominio() {
        return condominio;
    }

    public void setCondominio(Double condominio) {
        this.condominio = condominio;
    }

    public Integer getGaragem() {
        return garagem;
    }

    public void setGaragem(Integer garagem) {
        this.garagem = garagem;
    }

    public Integer getIs_mobiliado() {
        return is_mobiliado;
    }

    public void setIs_mobiliado(Integer is_mobiliado) {
        this.is_mobiliado = is_mobiliado;
    }

    public Integer getQuarto() {
        return quarto;
    }

    public void setQuarto(Integer quarto) {
        this.quarto = quarto;
    }

    public Integer getBanheiro() {
        return banheiro;
    }

    public void setBanheiro(Integer banheiro) {
        this.banheiro = banheiro;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getRua() {
        return rua;
    }

    public void setRua(String rua) {
        this.rua = rua;
    }

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeDouble(this.areaConstruida);
        dest.writeDouble(this.areaTotal);
        dest.writeString(this.bairro);
        dest.writeInt(this.banheiro);
        dest.writeString(this.cidade);
        dest.writeString(this.complemento);
        dest.writeString(this.condicao);
        dest.writeDouble(this.condominio);
        dest.writeString(this.estado);
        dest.writeInt(this.fk_proprietario_id);
        dest.writeInt(this.garagem);
        dest.writeInt(this.is_mobiliado);
        dest.writeInt(this.numero);
        dest.writeDouble(this.preco);
        dest.writeInt(this.quarto);
        dest.writeString(this.rua);
        dest.writeString(this.tipo);
        dest.writeString(this.topologia);
    }
}