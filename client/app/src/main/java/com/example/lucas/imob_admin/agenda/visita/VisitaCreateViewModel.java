package com.example.lucas.imob_admin.agenda.visita;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import androidx.annotation.NonNull;

import com.example.lucas.imob_admin.api.ApiClient;
import com.example.lucas.imob_admin.api.ApiService;
import com.example.lucas.imob_admin.api.RetrofitCallback;
import com.example.lucas.imob_admin.models.Visita;

public class VisitaCreateViewModel extends AndroidViewModel {
    private ApiService api = ApiClient.createService(ApiService.class);

    private MutableLiveData<Visita> mVisita = new MutableLiveData<>();
    private MutableLiveData<Boolean> loading = new MutableLiveData<>();
    private MutableLiveData<Boolean> error = new MutableLiveData<>();

    public VisitaCreateViewModel(@NonNull Application application) {
        super(application);
    }

    public void create(final Visita visita) {
        loading.setValue(true);
        api.createVisita(visita).enqueue(new RetrofitCallback<Visita>() {
            @Override
            public void onSuccess(Visita model) {
                mVisita.setValue(model);
                error.setValue(false);
            }

            @Override
            public void onFailure(int code, String msg) {
                error.setValue(true);
            }

            @Override
            public void onThrowable(Throwable t) {
                error.setValue(true);
            }

            @Override
            public void onFinish() {
                loading.setValue(false);
            }
        });
    }

    public MutableLiveData<Visita> getmVisita() {
        return mVisita;
    }

    public MutableLiveData<Boolean> getLoading() {
        return loading;
    }

    public MutableLiveData<Boolean> getError() {
        return error;
    }

}
