package com.example.lucas.imob_admin.cliente;

import android.app.Activity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.example.lucas.imob_admin.models.Cliente;
import com.example.lucas.imob_admin.R;
import com.example.lucas.imob_admin.utils.SimpleDividerItemDecoration;
import com.example.lucas.imob_admin.main.MainViewModel;

import java.util.List;

public class ClienteFragment extends Fragment implements View.OnClickListener, ClienteAdapter.ClickListener, ActionMode.Callback, SwipeRefreshLayout.OnRefreshListener {
    private static final String TAG = "ClienteFragment";
    public static final int FORM_REQUEST_CODE = 1;

    private MainViewModel viewModel;
    private SwipeRefreshLayout swipe;
    private RelativeLayout layout;
    private RecyclerView recyclerView;
    private ActionMode actionMode;
    private ClienteAdapter adapter;
    private FloatingActionButton fab;

    public ClienteFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_cliente, container, false);
        setHasOptionsMenu(true);

        setupViewModel();

        // SwipeRefreshLayout
        swipe = view.findViewById(R.id.swipeCliente);
        swipe.setOnRefreshListener(this);
        swipe.setColorSchemeResources(R.color.primaryColor,
                R.color.secondaryColor);

        layout = view.findViewById(R.id.clientesLayout);

        fab = view.findViewById(R.id.fab_cliente);
        fab.setOnClickListener(this);

        recyclerView = view.findViewById(R.id.recycler_cliente);
        recyclerView.setHasFixedSize(true);
        adapter = new ClienteAdapter(getActivity());
        adapter.setOnItemClickListener(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));

        return view;
    }

    private void setupViewModel() {
        viewModel = ViewModelProviders.of(getActivity()).get(MainViewModel.class);

        viewModel.getClientes().observe(this,
                new Observer<List<Cliente>>() {
                    @Override
                    public void onChanged(@Nullable List<Cliente> clientes) {
                        adapter.setItems(clientes);
                    }
                });

        viewModel.getLoading().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if (aBoolean) {
                    swipe.setRefreshing(true);
                } else {
                    swipe.setRefreshing(false);
                }
            }
        });

        viewModel.getError().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if (aBoolean) {
                    swipe.setRefreshing(false);
                    Snackbar.make(layout, "Erro", Snackbar.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onCallClick(int position) {
        Log.d(TAG, "onCallClick: " + adapter.getItem(position).getNome());
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + adapter.getItem(position).getTelefone()));
        startActivity(intent);
    }

    @Override
    public void onItemClick(int position, View v) {
        if(actionMode != null) {
            toggleSelection(position);
        } else {
            Intent intent = new Intent(getActivity(), ClienteUpdateActivity.class);
            Cliente clienteExtra = new Cliente(adapter.getItem(position).getNome(), adapter.getItem(position).getTelefone());
            clienteExtra.setId(adapter.getItem(position).getId());
            intent.putExtra(ClienteUpdateActivity.CLIENTE_EXTRA, clienteExtra);
            startActivityForResult(intent, FORM_REQUEST_CODE);
        }
    }

    @Override
    public void onItemLongClick(int position, View v) {
        if(actionMode == null) {
            actionMode = recyclerView.startActionMode(this);
            toggleSelection(position);
        }
    }

    private void toggleSelection(int pos) {
        adapter.toggleSelection(pos);
        int count = adapter.getSelectedItemCount();
        if (count == 0) {
            actionMode.finish();
            actionMode = null;
        } else {
            actionMode.setTitle(String.valueOf(count));
            actionMode.invalidate();
        }
    }

    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        MenuInflater menuInflater = mode.getMenuInflater();
        menuInflater.inflate(R.menu.cliente_menu, menu);
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        switch(item.getItemId()) {
            case R.id.action_cliente_delete:
                viewModel.deleteClientes(adapter.getSelectedClientes());
                mode.finish();
                return true;

            default:
                return false;
        }
    }

    @Override
    public void onDestroyActionMode(ActionMode mode) {
        actionMode = null;
        adapter.clearSelection();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab_cliente:
                Intent intent = new Intent(getActivity(), ClienteCreateActivity.class);
                startActivityForResult(intent, FORM_REQUEST_CODE);
                break;
        }
    }

    @Override
    public void onRefresh() {
        viewModel.listClientes();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == FORM_REQUEST_CODE) {
            if(resultCode == Activity.RESULT_OK){
                viewModel.listClientes();
            }
        }
    }
}
