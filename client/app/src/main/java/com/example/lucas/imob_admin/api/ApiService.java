package com.example.lucas.imob_admin.api;



import com.example.lucas.imob_admin.models.Cliente;
import com.example.lucas.imob_admin.models.Foto;
import com.example.lucas.imob_admin.models.Imovel;
import com.example.lucas.imob_admin.models.Visita;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {

    // [START] Cliente
    @GET("cliente")
    Call<List<Cliente>> listClientes();

    @GET("cliente/{id}")
    Call<Cliente> getCliente(@Path("id") int id);

    @POST("cliente")
    Call<Cliente> createCliente(@Body Cliente cliente);

    @DELETE("cliente/{id}")
    Call<String> deleteCliente(@Path("id") Integer id);

    @PUT("cliente/{id}")
    Call<Cliente> updateCliente(@Path("id") Integer id,
                                @Body Cliente cliente);
    // [END] Cliente

    // [START] Imovel
    @GET("imovel")
    Call<List<Imovel>> listImoveis();

    @GET("imovel/{id}")
    Call<Imovel> getImovel(@Path("id") int id);

    @POST("imovel")
    Call<Imovel> createImovel(@Body Imovel imovel);

    @DELETE("imovel/{id}")
    Call<String> deleteImovel(@Path("id") Integer id);

    @PUT("imovel/{id}")
    Call<Imovel> updateImovel(@Path("id") Integer id,
                              @Body Imovel imovel);
    // [END] Imovel

    // [START] Foto
    @GET("foto/{id}")
    Call<List<Foto>> getFotoUrls(@Path("id") Integer id);

    @GET("foto/first/{id}")
    Call<Foto> getFirstFotoUrl(@Path("id") Integer id);

    @POST("foto/{fk_foto_imovel_id}/{filename}")
    Call<ResponseBody> postFotos(@Path("fk_foto_imovel_id") Integer id,
                                 @Path("filename") String filename);
    // [END] Foto

    // [START] Visita
    @GET("visita/new")
    Call<List<Visita>> listNewVisitas();

    @GET("visita/old")
    Call<List<Visita>> listOldVisitas();

    @DELETE("visita/{id}")
    Call<String> deleteVisita(@Path("id") Integer id);

    @POST("visita")
    Call<Visita> createVisita(@Body Visita visita);
    // [END] Visita

}
