package com.example.lucas.imob_admin.imovel;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.lucas.imob_admin.R;
import com.example.lucas.imob_admin.models.Imovel;

public class ImovelUpdateActivity extends ImovelBaseActivity{
    public static final String IMOVEL_EXTRA = "imovel_obj";
    private Imovel imovel;
    public Integer id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle data = getIntent().getExtras();
        if (data != null) {
            imovel = (Imovel) data.getParcelable(IMOVEL_EXTRA);
            areaConstruida_imovel.setText(imovel.getAreaConstruida().toString());
            areaTotal_imovel.setText(imovel.getAreaTotal().toString());
            end_bairro.setText(imovel.getBairro());
            banheiro_np.setValue(imovel.getBanheiro());
            end_cidade.setText(imovel.getCidade().toString());
            end_comp.setText(imovel.getComplemento());
//        rb
            condominio_imovel.setText(imovel.getCondominio().toString());
            end_estado.setText(imovel.getEstado());
//        spinnProprietario
            garagem_np.setValue(imovel.getGaragem());
            mobiliado.setChecked(imovel.is_mobiliado == 1 ? true : false);
            end_numero.setText(imovel.getNumero().toString());
            preco_imovel.setText(imovel.getPreco().toString());
            quarto_np.setValue(imovel.getQuarto());
            end_rua.setText(imovel.getRua());
//        spinnTipo
//        spinnTopologia
        }
    }

    @Override
    public int getBtnLabel() {
        return R.string.btn_update;
    }

    @Override
    public Class<? extends ImovelViewModelBase> getViewModelClass() {
        return ImovelUpdateViewModel.class;
    }

    @Override
    public Imovel getImovel() {
        Imovel imovelEdited = new Imovel(
                Double.valueOf(areaConstruida_imovel.getText().toString()),
                Double.valueOf(areaTotal_imovel.getText().toString()),
                end_bairro.getText().toString(),
                banheiro_np.getValue(),
                end_cidade.getText().toString(),
                end_comp.getText().toString(),
                rb.getText().toString(),
                Double.valueOf(condominio_imovel.getText().toString()),
                end_estado.getText().toString(),
                proprietario.getId(),
                garagem_np.getValue(),
                mobiliado.isChecked() ? 1 : 0,
                Integer.valueOf(end_numero.getText().toString()),
                Double.valueOf(preco_imovel.getText().toString()),
                quarto_np.getValue(),
                end_rua.getText().toString(),
                spinnTipo.getSelectedItem().toString(),
                spinnTopologia.getSelectedItem().toString());
        imovelEdited.setId(imovel.getId());
        return imovelEdited;
    }
}