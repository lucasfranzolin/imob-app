package com.example.lucas.imob_admin.cliente;

import com.example.lucas.imob_admin.R;
import com.example.lucas.imob_admin.models.Cliente;

public class ClienteCreateActivity extends ClienteBaseActivity {
    @Override
    public int getBtnLabel() {
        return R.string.btn_create;
    }

    @Override
    public Class<? extends ClienteViewModelBase> getViewModelClass() {
        return ClienteCreateViewModel.class;
    }

    @Override
    public Cliente getCliente() {
        return new Cliente(nome.getText().toString(), telefone.getText().toString());
    }
}
