package com.example.lucas.imob_admin.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Visita {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("data")
    @Expose
    private String data;
    @SerializedName("horario")
    @Expose
    private String horario;
    @SerializedName("fk_cliente_id")
    @Expose
    private Integer clienteId;
    @SerializedName("fk_visita_imovel_id")
    @Expose
    private Integer imovelId;

    public Visita(String data, String horario, Integer clienteId, Integer imovelId) {
        this.data = data;
        this.horario = horario;
        this.clienteId = clienteId;
        this.imovelId = imovelId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public Integer getClienteId() {
        return clienteId;
    }

    public void setClienteId(Integer clienteId) {
        this.clienteId = clienteId;
    }

    public Integer getImovelId() {
        return imovelId;
    }

    public void setImovelId(Integer imovelId) {
        this.imovelId = imovelId;
    }

}
