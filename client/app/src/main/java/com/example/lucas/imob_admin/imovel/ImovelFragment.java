package com.example.lucas.imob_admin.imovel;

import android.app.Activity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.lucas.imob_admin.models.Imovel;
import com.example.lucas.imob_admin.R;
import com.example.lucas.imob_admin.main.MainViewModel;

import java.util.List;

public class ImovelFragment extends Fragment implements View.OnClickListener, ImovelAdapter.ClickListener, SwipeRefreshLayout.OnRefreshListener {
    private static final String TAG = "ImovelFragment";
    public static final int FORM_IMOVEL_REQUEST_CODE = 1;
    public static final String IMOVEL_FRAGMENT_KEY = "imovel_id";

    private MainViewModel viewModel;
    private SwipeRefreshLayout swipe;
    private RecyclerView recyclerView;
    private ImovelAdapter adapter;
    private FloatingActionButton fab;

    public ImovelFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_imovel, container, false);
        setHasOptionsMenu(true);

        setupViewModel();

        swipe = view.findViewById(R.id.swipeImovel);
        swipe.setOnRefreshListener(this);
        swipe.setColorSchemeResources(R.color.primaryColor,
                R.color.secondaryColor);

        fab = view.findViewById(R.id.fab_imovel);
        fab.setOnClickListener(this);

        recyclerView = view.findViewById(R.id.recycler_imovel);
        recyclerView.setHasFixedSize(true);
        adapter = new ImovelAdapter(getActivity());
        adapter.setOnItemClickListener(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        return view;
    }

    private void setupViewModel() {
        viewModel = ViewModelProviders.of(getActivity()).get(MainViewModel.class);

        viewModel.getImoveis().observe(this,
                new Observer<List<Imovel>>() {
                    @Override
                    public void onChanged(@Nullable List<Imovel> imoveis) {
                        adapter.setItems(imoveis);
                    }
                });

        viewModel.getLoading().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if (aBoolean) {
                    swipe.setRefreshing(true);
                } else {
                    swipe.setRefreshing(false);
                }
            }
        });

        viewModel.getError().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if (aBoolean) swipe.setRefreshing(false);
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab_imovel:
                Intent intent = new Intent(getActivity(), ImovelCreateActivity.class);
                startActivityForResult(intent, FORM_IMOVEL_REQUEST_CODE);
                break;
        }
    }

    @Override
    public void onItemClick(int position, View v) {
        Intent intent = new Intent(getActivity(), ImovelDetailActivity.class);
        intent.putExtra(IMOVEL_FRAGMENT_KEY, adapter.getItem(position).getId());
        startActivityForResult(intent, v.getId());
    }

    @Override
    public void onRefresh() {
        viewModel.listImoveis();
    }

    @Override
    public void onResume() {
        super.onResume();
        viewModel.listImoveis();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == FORM_IMOVEL_REQUEST_CODE) {
            if(resultCode == Activity.RESULT_OK){
                viewModel.listImoveis();
            }
        }
    }
}
