package com.example.lucas.imob_admin.cliente;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.lucas.imob_admin.models.Cliente;
import com.example.lucas.imob_admin.R;

import java.util.ArrayList;
import java.util.List;

import static java.util.Locale.getDefault;

public class ClienteAdapter extends RecyclerView.Adapter<ClienteAdapter.ViewHolder> {

    private static ClickListener clickListener;

    public interface ClickListener {
        void onCallClick(int position);
        void onItemClick(int position, View v);
        void onItemLongClick(int position, View v);
    }

    public void setOnItemClickListener(ClickListener clickListener) {
        ClienteAdapter.clickListener = clickListener;
    }

    private List<Cliente> clientes;
    private Context context;
    private SparseBooleanArray selectedItems;
    private List<Cliente> selectedClientes = new ArrayList<>();

    public ClienteAdapter(Context context) {
        this.context = context;
        this.clientes = new ArrayList<>();
        selectedItems = new SparseBooleanArray();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        TextView txtNome;
        ImageView phone;

        public ViewHolder(@NonNull final View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);

            txtNome = itemView.findViewById(R.id.cliente_nome);
            phone = itemView.findViewById(R.id.btn_call);
            phone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickListener.onCallClick(getAdapterPosition());
                }
            });
        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(getAdapterPosition(), v);
        }

        @Override
        public boolean onLongClick(View v) {
            clickListener.onItemLongClick(getAdapterPosition(), v);
            return true;
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View clienteView = inflater.inflate(R.layout.list_item_cliente, viewGroup, false);
        return new ViewHolder(clienteView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int i) {
        final Cliente currentCliente = clientes.get(i);
        viewHolder.txtNome.setText(currentCliente.getNome());
        viewHolder.itemView.setActivated(selectedItems.get(i, false));
    }

    @Override
    public int getItemCount() {
        return clientes.size();
    }

    public void setItems(List<Cliente> clientes) {
        this.clientes = clientes;
        notifyDataSetChanged();
    }

    public Cliente getItem(int position) {
        return clientes.get(position);
    }

    public void toggleSelection(int pos) {
        if (selectedItems.get(pos, false)) {
            selectedItems.delete(pos);
            selectedClientes.remove(getItem(pos));
        } else {
            selectedItems.put(pos, true);
            selectedClientes.add(getItem(pos));
        }
        notifyItemChanged(pos);
    }

    public int getSelectedItemCount() {
        return selectedItems.size();
    }

    public void clearSelection() {
        selectedItems.clear();
        selectedClientes.clear();
        notifyDataSetChanged();
    }

    public List<Cliente> getSelectedClientes() {
        return selectedClientes;
    }

}
