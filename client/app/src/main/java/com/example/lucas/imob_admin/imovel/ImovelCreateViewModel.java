package com.example.lucas.imob_admin.imovel;

import android.app.Application;
import android.net.Uri;
import android.util.Log;

import androidx.annotation.NonNull;

import com.example.lucas.imob_admin.api.RetrofitCallback;
import com.example.lucas.imob_admin.models.Imovel;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.util.List;

import okhttp3.ResponseBody;

public class ImovelCreateViewModel extends ImovelViewModelBase {
    public ImovelCreateViewModel(@NonNull Application application) {
        super(application);
    }

    @Override
    public void submit(Imovel imovel) {
        loading.setValue(true);
        api.createImovel(imovel).enqueue(new RetrofitCallback<Imovel>() {
            @Override
            public void onSuccess(Imovel model) {
                mImovel.setValue(model);
                error.setValue(false);
                if (imageUris.size() > 0) postImages(model.getId(), imageUris);
            }

            @Override
            public void onFailure(int code, String msg) {
                error.setValue(true);
            }

            @Override
            public void onThrowable(Throwable t) {
                error.setValue(true);
            }

            @Override
            public void onFinish() {
                loading.setValue(false);
            }
        });
    }

    private void postImages(final int imovel_id, final List<Uri> uris) {
        Uri fileUri = uris.get(0);
        File file = new File(fileUri.getPath());
        final String fileName = file.getName();

        FirebaseStorage storage = FirebaseStorage.getInstance("gs://imob-media");
        // Create a storage reference from our app
        StorageReference storageRef = storage.getReference();
        // Create a child reference
        StorageReference imoveisRef = storageRef.child("imoveis");
        // Create a reference to 'images/mountains.jpg'
        StorageReference newFolderRef = imoveisRef.child(imovel_id + "/" + fileName);
        UploadTask uploadTask = newFolderRef.putFile(fileUri);
        // Register observers to listen for when the download is done or if it fails
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                error.setValue(true);
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                loading.setValue(false);
                api.postFotos(imovel_id, fileName).enqueue(new RetrofitCallback<ResponseBody>() {
                    @Override
                    public void onSuccess(ResponseBody model) {
                        error.setValue(false);
                    }

                    @Override
                    public void onFailure(int code, String msg) {
                        error.setValue(true);
                    }

                    @Override
                    public void onThrowable(Throwable t) {
                        error.setValue(true);
                    }

                    @Override
                    public void onFinish() {
                        loading.setValue(false);
                    }
                });
            }
        });
    }
}
