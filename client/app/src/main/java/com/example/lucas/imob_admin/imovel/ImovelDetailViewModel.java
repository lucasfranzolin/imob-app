package com.example.lucas.imob_admin.imovel;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import androidx.annotation.NonNull;

import com.example.lucas.imob_admin.api.ApiClient;
import com.example.lucas.imob_admin.api.ApiService;
import com.example.lucas.imob_admin.api.RetrofitCallback;
import com.example.lucas.imob_admin.models.Cliente;
import com.example.lucas.imob_admin.models.Foto;
import com.example.lucas.imob_admin.models.Imovel;

import java.util.List;

public class ImovelDetailViewModel extends AndroidViewModel {
    private ApiService api = ApiClient.createService(ApiService.class);

    private MutableLiveData<List<Foto>> mFoto = new MutableLiveData<>();
    private MutableLiveData<Imovel> mImovel = new MutableLiveData<>();
    private MutableLiveData<Cliente> mProprietario = new MutableLiveData<>();
    private MutableLiveData<Boolean> loading = new MutableLiveData<>();
    private MutableLiveData<Boolean> error = new MutableLiveData<>();
    private MutableLiveData<Boolean> deleted = new MutableLiveData<>();

    public ImovelDetailViewModel(@NonNull Application application) {
        super(application);
    }

    public void getImovelDetails(int id) {
        getUrlsByImovelId(id);
        getImovel(id);
    }

    public void getUrlsByImovelId(final int id) {
        loading.setValue(true);
        api.getFotoUrls(id).enqueue(new RetrofitCallback<List<Foto>>() {
            @Override
            public void onSuccess(List<Foto> model) {
                mFoto.setValue(model);
                error.setValue(false);
            }

            @Override
            public void onFailure(int code, String msg) {
                error.setValue(true);
            }

            @Override
            public void onThrowable(Throwable t) {
                error.setValue(true);
            }

            @Override
            public void onFinish() {
                loading.setValue(false);
            }
        });
    }

    public void getImovel(final int id) {
        loading.setValue(true);
        api.getImovel(id).enqueue(new RetrofitCallback<Imovel>() {
            @Override
            public void onSuccess(Imovel model) {
                mImovel.setValue(model);
                error.setValue(false);
            }

            @Override
            public void onFailure(int code, String msg) {
                error.setValue(true);
            }

            @Override
            public void onThrowable(Throwable t) {
                error.setValue(true);
            }

            @Override
            public void onFinish() {
                loading.setValue(false);
            }
        });
    }

    public void getProprietario(final int id) {
        api.getCliente(id).enqueue(new RetrofitCallback<Cliente>() {
            @Override
            public void onSuccess(Cliente model) {
                mProprietario.setValue(model);
                error.setValue(false);
            }

            @Override
            public void onFailure(int code, String msg) {
                error.setValue(true);
            }

            @Override
            public void onThrowable(Throwable t) {
                error.setValue(true);
            }

            @Override
            public void onFinish() {
                loading.setValue(false);
            }
        });
    }

    public void deleteImovel(final int id) {
        loading.setValue(true);
        api.deleteImovel(id).enqueue(new RetrofitCallback<String>() {
            @Override
            public void onSuccess(String model) {
                deleted.setValue(false);
                mImovel.setValue(null);
                error.setValue(false);
            }

            @Override
            public void onFailure(int code, String msg) {
                deleted.setValue(true);
            }

            @Override
            public void onThrowable(Throwable t) {
                deleted.setValue(true);
            }

            @Override
            public void onFinish() {
                loading.setValue(false);
            }
        });
    }

    public MutableLiveData<List<Foto>> getmFoto() {
        return mFoto;
    }

    public MutableLiveData<Imovel> getmImovel() {
        return mImovel;
    }

    public MutableLiveData<Cliente> getmProprietario() {
        return mProprietario;
    }

    public MutableLiveData<Boolean> getLoading() {
        return loading;
    }

    public MutableLiveData<Boolean> getError() {
        return error;
    }

    public MutableLiveData<Boolean> getDeleted() {
        return deleted;
    }
}
