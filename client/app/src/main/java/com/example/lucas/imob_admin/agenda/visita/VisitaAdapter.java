package com.example.lucas.imob_admin.agenda.visita;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.lucas.imob_admin.R;
import com.example.lucas.imob_admin.api.ApiClient;
import com.example.lucas.imob_admin.api.ApiService;
import com.example.lucas.imob_admin.api.RetrofitCallback;
import com.example.lucas.imob_admin.models.Cliente;
import com.example.lucas.imob_admin.models.Imovel;
import com.example.lucas.imob_admin.models.Visita;

import java.util.ArrayList;
import java.util.List;

public class VisitaAdapter extends RecyclerView.Adapter<VisitaAdapter.ViewHolder> {
    private ApiService api = ApiClient.createService(ApiService.class);

    private static ClickListener clickListener;

    public interface ClickListener {
        void onItemClick(int position, View v);
        void onItemDeleteClick(int position);
    }

    public void setOnItemClickListener(ClickListener clickListener) {
        VisitaAdapter.clickListener = clickListener;
    }

    private List<Visita> visitas;
    private Context context;

    public VisitaAdapter(Context context) {
        this.context = context;
        this.visitas = new ArrayList<>();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView data, horario, cliente, endereco;
        ImageButton delete;

        public ViewHolder(@NonNull final View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            data = itemView.findViewById(R.id.txt_visita_data);
            horario = itemView.findViewById(R.id.txt_visita_horario);
            cliente = itemView.findViewById(R.id.txt_visita_cliente);
            endereco = itemView.findViewById(R.id.txt_visita_endereco);
            delete = itemView.findViewById(R.id.btn_delete_visita);
            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickListener.onItemDeleteClick(getAdapterPosition());
                }
            });
        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(getAdapterPosition(), v);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View itemView = inflater.inflate(R.layout.list_item_visita, viewGroup, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        final Visita currentVisita = visitas.get(i);
        viewHolder.data.setText(currentVisita.getData());
        viewHolder.horario.setText(currentVisita.getHorario());
        setVisitante(currentVisita.getClienteId(), viewHolder.cliente);
        setImovelEndereco(currentVisita.getImovelId(), viewHolder.endereco);
    }

    private void setVisitante(final int id, final TextView textView) {
        api.getCliente(id).enqueue(new RetrofitCallback<Cliente>() {
            @Override
            public void onSuccess(Cliente model) {
                textView.setText(model.getNome());
            }

            @Override
            public void onFailure(int code, String msg) {

            }

            @Override
            public void onThrowable(Throwable t) {

            }

            @Override
            public void onFinish() {

            }
        });
    }

    private void setImovelEndereco(final int id, final TextView textView) {
        api.getImovel(id).enqueue(new RetrofitCallback<Imovel>() {
            @Override
            public void onSuccess(Imovel model) {
                textView.setText(model.getRua() + ", " +
                                model.getNumero().toString() + " - " +
                                model.getBairro() + ", " +
                                model.getCidade() + " - " +
                                model.getEstado()
                );
            }

            @Override
            public void onFailure(int code, String msg) {

            }

            @Override
            public void onThrowable(Throwable t) {

            }

            @Override
            public void onFinish() {

            }
        });
    }

    public void setItems(List<Visita> visitas) {
        this.visitas = visitas;
        notifyDataSetChanged();
    }

    public Visita getItem(int position) {
        return visitas.get(position);
    }

    @Override
    public int getItemCount() {
        return visitas.size();
    }

}
