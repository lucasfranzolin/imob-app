package com.example.lucas.imob_admin.agenda;

import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.lucas.imob_admin.R;
import com.example.lucas.imob_admin.agenda.historico.HistoricoFragment;
import com.example.lucas.imob_admin.agenda.visita.VisitasFragment;
import com.example.lucas.imob_admin.utils.SectionsPagerAdapter;

public class AgendaFragment extends Fragment {
    private static final String TAG = "AgendaFragment";

    private FragmentManager fragManager; //If using fragments from support v4

    private ViewPager viewPager;
    private TabLayout tabLayout;

    public AgendaFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_agenda, container, false);

        viewPager = view.findViewById(R.id.container);
        setupViewPager(viewPager);

        tabLayout = view.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();

        return view;
    }

    private void setupTabIcons() {
        tabLayout.getTabAt(0).setText("Visitas");
        tabLayout.getTabAt(1).setText("Historico");
    }

    private void setupViewPager(ViewPager viewPager) {
        SectionsPagerAdapter adapter = new SectionsPagerAdapter(getChildFragmentManager());
        adapter .addFragment(new VisitasFragment()); // 0
        adapter .addFragment(new HistoricoFragment()); // 1
        viewPager.setAdapter(adapter);
    }

}
