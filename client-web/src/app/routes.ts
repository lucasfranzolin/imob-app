import { Routes } from '@angular/router';

import { ReportsComponent } from './sections/reports/reports.component';

export const appRoutes: Routes = [
    { path: 'reports', component: ReportsComponent}
];