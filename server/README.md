### Local Development ###

1) Keep the Cloud SQL Proxy running the entire time you test your application locally.\n
```console
$ ./cloud_sql_proxy -instances="steam-bee-231721:southamerica-east1:imob-db"=tcp:3306
```

2) To initialize the SQL database, enter this command with the Cloud SQL proxy running:
```console
$ npm run init-cloudsql

```

3) Start a local web server using npm (port 3000)
```console
$ npm start

```

### Deploy to production ###

1) Deploy the app to the App Engine standard environment. The new deployment creates a new version of your app and promotes it to the default version. The older versions of your app remain.
```console
$ gcloud app deploy

```

2) Access your live web server through https://steam-bee-231721.appspot.com