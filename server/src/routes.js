'use strict';

const express = require('express');
const router = express.Router();
const logging = require('./lib/logging');

// Controllers
const ClienteController = require("./controllers/ClienteController");
const VisitaController = require("./controllers/VisitaController");
const ImovelController = require("./controllers/ImovelController");
const FotoController = require("./controllers/FotoController");

// Routes
router.use('/cliente', ClienteController);
router.use('/visita', VisitaController);
router.use('/imovel', ImovelController);
router.use('/foto', FotoController);

// Add the error logger after all middleware and routes so that
// it can log errors from the whole application. Any custom error
// handlers should go after this.
// [START errors]
router.use(logging.errorLogger);

// Basic 404 handler
router.use((req, res) => {
    res.status(404).send('Not Found');
});

// Basic error handler
router.use((err, req, res) => {
    console.error(err);
    res.status(500).send(err.response || 'Something broke!');
});
// [END errors]

module.exports = router;