const config = require('../../config');
const connection = require('../lib/cloud-sql');
const foto_table = `\`${config.MYSQL_DATABASE}\`.\`foto\``;
const visita_table = `\`${config.MYSQL_DATABASE}\`.\`visita\``;
const imovel_table = `\`${config.MYSQL_DATABASE}\`.\`imovel\``;

async function _delete(id, cb) {
    await connection.query(
        `DELETE FROM ${foto_table} WHERE \`fk_foto_imovel_id\` = ?`,
        id
    )
    .then(_deleteFromVisita(id, cb))
    .catch((err) => {
        console.log('error delete foto', err);
    });
}

async function _deleteFromVisita(id, cb) {    
    await connection.query(
        `DELETE FROM ${visita_table} WHERE \`fk_visita_imovel_id\` = ?`,
        id
    )
    .then(_deleteFromImovel(id, cb))
    .catch((err) => {
        console.log('error delete visita', err);
    });
}

async function _deleteFromImovel(id, cb) {    
    await connection.query(
        `DELETE FROM ${imovel_table} WHERE \`id\` = ?`,
        id,
        (err, result) => {
            cb(err, result);
        }
    );
}

module.exports = {
    _delete
};