const config = require('../../config');
const connection = require('../lib/cloud-sql');
const storage = require('../helpers/google-cloud-storage');
const table = `\`${config.MYSQL_DATABASE}\`.\`foto\``;

function getPhotosByImovelId(id, cb) {
    connection.query(
        `SELECT \`filename\` FROM ${table} WHERE \`fk_foto_imovel_id\` = ?`,
        id,
        (err, result) => {
            if (err) {
                cb({ code: 404, message: 'Not found' });
            }
            if (result.length > 0) {
                var filenames = [];
                for (var i = 0; i < result.length; i++) {
                    filenames.push({ url: storage.getPublicUrl(config.CLOUD_BUCKET, id, result[i].filename) });
                }
                cb(null, filenames);
            }
            return;
        }
    );
}

function getSinglePhotoByImovelId(id, cb) {
    connection.query(
        `SELECT \`filename\` FROM ${table} WHERE \`fk_foto_imovel_id\` = ? LIMIT 1`,
        id,
        (err, result) => {
            if (err) {
                cb({ code: 404, message: 'Not found' });
            }
            if (result.length > 0) {
                const resp = { url: storage.getPublicUrl(config.CLOUD_BUCKET, id, result[0].filename) };
                console.log(resp);
                cb(null, resp);
            }
            return;
        }
    );
}

module.exports = {
    getPhotosByImovelId,
    getSinglePhotoByImovelId
};