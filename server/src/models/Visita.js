const config = require('../../config');
const connection = require('../lib/cloud-sql');
const table = `\`${config.MYSQL_DATABASE}\`.\`visita\``;

function listNewAsc(cb) {
    connection.query(
        `SELECT * FROM ${table} WHERE ${table}.\`data\` >= CURDATE() ORDER BY ${table}.\`data\` ASC, ${table}.\`horario\` ASC`,
        (err, results) => {
            if (err) {
                cb({ code: 404, message: 'Not found' });
                return;
            }
            cb(null, results);
        }
    );
}

function listHistoricAsc(cb) {
    connection.query(
        `SELECT * FROM ${table} WHERE ${table}.\`data\` <= CURDATE() ORDER BY ${table}.\`data\` ASC, ${table}.\`horario\` ASC`,
        (err, results) => {
            if (err) {
                cb({ code: 404, message: 'Not found' });
                return;
            }
            cb(null, results);
        }
    );
}

module.exports = {
    listNewAsc,
    listHistoricAsc
};