const router = require('express').Router();
const Foto = require('../models/Foto');	
const Dao = require('../lib/dao');
const FotoDao = new Dao('foto');
const Multer = require('multer');

router.post('/:fk_foto_imovel_id/:filename', (req, res) => {
  FotoDao.create(req.params, (err, foto) => {
    if (err) return res.json(err);
    res.json(foto);
  });
});

router.get('/:id', (req, res) => {
    Foto.getPhotosByImovelId(req.params.id, (err, fotos) => {
        if (err) return res.json(err);
        res.json(fotos);
    });
});

router.get('/first/:id', (req, res) => {
  Foto.getSinglePhotoByImovelId(req.params.id, (err, foto) => {
      if (err) return res.json(err);
      res.json(foto);
  });
});

module.exports = router;