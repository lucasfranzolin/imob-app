const router = require('express').Router();
const Dao = require('../lib/dao');
const VisitaDao = new Dao('visita');
const Visita = require('../models/Visita');

/*
** "data": YYYY-MM-DD
** "horario": HH:MM
*/
router.post('/', (req, res) => {
    VisitaDao.create(req.body, (err, visita) => {
        if (err) return res.json(err);
        res.json(visita);
    });
});

router.get('/new', (req, res) => {
    Visita.listNewAsc((err, visitas) => {
        if (err) return res.json(err);
        res.json(visitas);
    });
});

router.get('/old', (req, res) => {
    Visita.listHistoricAsc((err, visitas) => {
        if (err) return res.json(err);
        res.json(visitas);
    });
});

router.get('/:id', (req, res) => {
    VisitaDao.readBy(req.params.id, (err, visita) => {
        if (err) return res.json(err);
        res.json(visita);
    });
});

router.put('/:id', (req, res, next) => {
    VisitaDao.update(req.params.id, req.body, (err, visita) => {
        if (err) return res.json(err);
        res.json(visita);
    });
});

router.delete('/:id', (req, res, next) => {
    VisitaDao._delete(req.params.id, (err) => {
        if (err) {
            next(err);
            return;
        }
        res.status(200).send('OK');
    });
});

module.exports = router;