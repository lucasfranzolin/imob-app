const router = require('express').Router();
const Dao = require('../lib/dao');
const ImovelDao = new Dao('imovel');
const Imovel = require('../models/Imovel');

router.post('/', (req, res) => {
    ImovelDao.create(req.body, (err, imovel) => {
        if (err) return res.json(err);
        res.json(imovel);
    });
});

router.get('/', (req, res) => {
    ImovelDao.list((err, imoveis) => {
        if (err) return res.json(err);
        res.json(imoveis);
    });
});

router.get('/:id', (req, res) => {
    ImovelDao.readBy(req.params.id, (err, imovel) => {
        if (err) return res.json(err);
        res.json(imovel);
    });
});

router.put('/:id', (req, res) => {
    ImovelDao.update(req.params.id, req.body, (err, imovel) => {
        if (err) return res.json(err);
        res.json(imovel);
    });
});

router.delete('/:id', async (req, res, next) => {
    await Imovel._delete(req.params.id, (err, imoveis) => {
        if (err) {
            res.send(err);
            return;
        }
        res.json(imoveis);
    });
});

module.exports = router;