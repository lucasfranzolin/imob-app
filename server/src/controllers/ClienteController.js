const router = require('express').Router();
const config = require('../../config');
const db = require('../lib/cloud-sql');
const Dao = require('../lib/dao');
const ClienteDao = new Dao('cliente');
const table = `\`${config.MYSQL_DATABASE}\`.\`cliente\``;

router.post('/', (req, res) => {
    ClienteDao.create(req.body, (err, cliente) => {
        if (err) return res.json(err);
        res.json(cliente);
    });
});

router.get('/', (req, res) => {
    ClienteDao.list((err, clientes) => {
        if (err) return res.json(err);
        res.json(clientes);
    });
});

router.get('/:id', (req, res) => {
    ClienteDao.readBy(req.params.id, (err, cliente) => {
        if (err) return res.json(err);
        res.json(cliente);
    });
});

router.put('/:id', (req, res) => {
    ClienteDao.update(req.params.id, req.body, (err, cliente) => {
        if (err) return res.json(err);
        res.json(cliente);
    });
});

router.delete('/:id', (req, res, next) => {
    ClienteDao._delete(req.params.id, (err) => {
        if (err) {
            next(err);
            return
        }
        res.status(200).send('OK');
    });
});

router.post('/filter', (req, res) => {
    db.query(
        `SELECT * FROM ${table} WHERE \`nome\` LIKE ?`,
        req.body.nome,
        (err, result) => {
            if (!err && !result.length) throw err;
            res.status(200).json(result);
        }
    );
})

module.exports = router;