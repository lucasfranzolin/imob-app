const config = require('../../config');
const mysql = require('mysql');
const options = {
    user: config.MYSQL_USER,
    password: config.MYSQL_PASSWORD,
    database: config.MYSQL_DATABASE
    // socketPath: `/cloudsql/${config.INSTANCE_CONNECTION_NAME}` // comment this line to run locally
};

const connection = mysql.createConnection(options);

connection.connect((err) => {
    if (err) console.log(err);
    else console.log('Connected to imob-db');
});

module.exports = connection;