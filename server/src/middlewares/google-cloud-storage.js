const config = require('../../config');
const gcsHelpers = require('../helpers/google-cloud-storage');

const fs = require('fs');

const { storage } = gcsHelpers;

const DEFAULT_BUCKET_NAME = config.GCLOUD_PROJECT;

function base64_decode(base64str, file) {
    // create buffer object from base64 encoded string, it is important to tell the constructor that the string is base64 encoded
    var bitmap = new Buffer(base64str, 'base64');
    // write buffer to file
    return fs.writeFileSync(file, bitmap);
}

/**
 * Middleware for uploading file to GCS.
 * @param {Object} req
 * @param {Object} res
 * @param {Function} next
 * @return {*}
 */
exports.sendUploadToGCS = (req, res, next) => {
    if (!req.file) {
        return next();
    }

    const bucketName = req.body.bucketName || DEFAULT_BUCKET_NAME;
    const bucket = storage.bucket(bucketName);
    const imovelId = req.params.id;

    const base64str = req.query.imgb64;
    const gcsFileName = `${Date.now()}`;
    mFile = base64_decode(base64str, gcsFileName);

    console.log('base64str = ' + base64str);
    console.log('typeOf(mFile) = ' + typeof(mFile));

    // const file = bucket.file(gcsFileName);

    // const stream = file.createWriteStream({
    //     metadata: {
    //         contentType: mFile.mimetype,
    //     },
    // });

    // stream.on('error', (err) => {
    //     mFile.cloudStorageError = err;
    //     console.log('error stream.on', err);
    //     next(err);
    // });

    // stream.on('finish', () => {
    //     mFile.cloudStorageObject = gcsFileName;
    //     console.log('on finish');
    //     return file.makePublic()
    //     .then(() => {
    //         mFile.gcsUrl = gcsHelpers.getPublicUrl(bucketName, imovelId, gcsFileName);
    //         next();
    //     });
    // });

    // stream.end(mFile.buffer);
};