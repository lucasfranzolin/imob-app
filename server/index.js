'use strict';

const express = require('express');
const config = require('./config');
const logging = require('./src/lib/logging');
const bodyParser = require('body-parser');
const routes = require('./src/routes');

// Initialize App
const app = express();

// Add the request logger before anything else so that it can
// accurately log requests.
app.use(logging.requestLogger);

// Middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Routes
app.use('/', routes);

if (module === require.main) {
    // Start the server
    const server = app.listen(config.PORT, () => {
        const port = server.address().port;
        console.log(`App listening on port ${port}`);
    });
}

module.exports = app;