### Local Development ###

1) Start the Cloud SQL at https://console.cloud.google.com/sql/instances/imob-db/connections?project=steam-bee-231721

2) Keep the Cloud SQL Proxy running the entire time you test your application locally.
 ```console
$ ./cloud_sql_proxy -instances="steam-bee-231721:us-east1:imoveis-lindoia"=tcp:3306
```

3) Comment line ```socketPath: `/cloudsql/${config.INSTANCE_CONNECTION_NAME}````of ```/src/lib/cloud-sql.js``` file

4) Start a local web server using npm (http://localhost:3000/)
```console
$ npm start

```

### Deploy to production ###

1) Deploy the app to the App Engine standard environment. The new deployment creates a new version of your app and promotes it to the default version. The older versions of your app remain.
```console
$ gcloud app deploy --version dev

```

2) Access your live web server through https://steam-bee-231721.appspot.com

3) When you are finished working for the day, you can spin down your instance with the following command:
```console
$ gcloud app versions stop dev

```

4) And you can start it back up just as easily:
```console
$ gcloud app versions start dev

```

5) Following steps 3 and 4 you can save 85% of the costs